
#include <iostream>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/LaserScan.h>

#include <math.h>

const int MSG_QUEUE_SIZE = 5;

void lidarCallback(const sensor_msgs::LaserScan::ConstPtr& scan) {
	std::cout << "GOT SCAN: " << scan->angle_min << " " << scan->angle_max << " " << scan->angle_increment << std::endl;
	std::cout << scan->range_min << " " << scan->range_max << std::endl;
	double inc =  scan->angle_increment;
	std::cout << scan->ranges.size() << std::endl << std::endl;

	double ang = 0;
	double r = 0;
	double x = 0;
	double y = 0;
	for (int n = 0; n < scan->ranges.size(); n++) {
		r = scan->ranges[n];
		x = r*cos(ang);
		y = r*sin(ang);
		std::cout << "(" << x << ", " << y << ")" << std::endl;
		ang += inc;
	}
	// scan->angle_min
	// ROS_INFO("Got msg: [%s]", msg->data.c_str());
}

int main(int argc, char** argv) {
	std::cout << "hello" << std::endl;
	ros::init(argc, argv, "test");
	ros::NodeHandle handle;
	ros::Subscriber sub = handle.subscribe("/scan", MSG_QUEUE_SIZE, lidarCallback);
	// ros::start();

	ros::spin();

	return 0;
}
