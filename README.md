## C++ module for using hls-lfcd-lds lidar (TurtleBot3 lidar) for proximity sensing.

Module listens to `/scan` messages from ROS. Check `lidar.hh` for usage.

### Requirements

Module requires ROS to work (tested with ROS Melodic). CMake files assume ROS is installed at `/opt/ros/melodic`.

Build ROS-package: [https://github.com/ROBOTIS-GIT/hls_lfcd_lds_driver.git]()

Launch with `roslaunch hls_lfcd_lds_driver hlds_laser.launch`.
