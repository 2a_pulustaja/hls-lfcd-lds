#ifndef _LIDAR_HH_
#define _LIDAR_HH_

#include <iostream>
#include <thread>

#include <functional>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/LaserScan.h>

#include <math.h>

#include <map>

#include <mutex>
#include <atomic>

class Lidar {
	static const int MSG_QUEUE_SIZE = 3;

public: // types and vars
	struct Point {
		int x;
		int y;
		Point(): x(0), y(0) { }
	};
	struct DPoint {
		double x;
		double y;
		DPoint(): x(0.0), y(0.0) { }
	};

public: // functions
	Lidar();
	// argc and argv are command line parameters that are needed for ros
	// initialization
	// rMin/rMax are robot bounding box corner coordinates. cMin/cMax are
	// corners of a bounding box within which robot is considered to
	// collide (not in  use at the moment). pMin/pMax are corners of a
	// bounding box within which robot is in proximity of an obect.
	// Coordinates are in millimeters in respect to the lidar origin.
	Lidar(int &argc, char **argv, Point &rMin, Point &rMax, Point &cMin, Point &cMax, Point &pMin, Point &pMax);
	~Lidar();
	// Returns a byte indicating where around the robot oject are seen
	// within proximity bounding box.
	/*
	x(pMin)--+----------------+---------o
	|        |                |         |
	|   1    |       4        |    2    |
	|        |                |         |
	|   x(cMin)-  -  -  -  -  |  - o    |
	|   |    |                |    |    |
	|        |                |         |
	|--------x(rMin)----------+---------|
	|        |                |         |
	|   |    |   __           |    |    |
	|        |  /  \          |         |
	|        |  \  / ----Y    |         |
	|   |    |   \/ |         |    |    |
	|  64    |      |         |    128  |
	|        |      |         |         |
	|   |    |      X         |    |    |
	|        |                |         |
	|--------+----------------x(rMax)   |
	|   |    |                |    |    |
	|        |                |         |
	|        |                |         |
	|   o-  -| -  -  -  -  -  |  - x(cMax)
	|        |                |         |
	|   8    |       32       |    16   |
	0--------+----------------+---------x(pMax)
	*/
	char checkProximity();

	// Callback that handles incoming ROS scan messages
	void lidarCallback(const sensor_msgs::LaserScan::ConstPtr& scan);

private:
	int argc;
	char *argv;
	DPoint rMin;
	DPoint rMax;
  DPoint cMin;
	DPoint cMax;
	DPoint pMin;
	DPoint pMax;
	char proximity;
	std::mutex proximity_mutex;
	Point obstaclePoint;
	std::thread ros_thread;

private: // functions
	void runOnThread();
};

#endif // _LIDAR_HH_
