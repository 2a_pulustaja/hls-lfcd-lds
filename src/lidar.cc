#include "lidar.hh"

#include <iostream>
#include <thread>

#include <functional>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/LaserScan.h>

#include <math.h>

#include <set>

Lidar::Lidar() {
}

Lidar::Lidar(int &argc, char **argv, Point &rMin, Point &rMax, Point &cMin, Point &cMax, Point &pMin, Point &pMax) {
	// this.argc = argc;
	// this->argv = argv;
	this->rMin = DPoint();
	this->rMax = DPoint();
	this->cMin = DPoint();
	this->cMax = DPoint();
	this->pMin = DPoint();
	this->pMax = DPoint();
	this->rMin.x = ((double)rMin.x)/1000;
	this->rMin.y = ((double)rMin.y)/1000;
	this->rMax.x = ((double)rMax.x)/1000;
	this->rMax.y = ((double)rMax.y)/1000;
	this->cMin.x = ((double)cMin.x)/1000;
	this->cMin.y = ((double)cMin.y)/1000;
	this->cMax.x = ((double)cMax.x)/1000;
	this->cMax.y = ((double)cMax.y)/1000;
	this->pMin.x = ((double)pMin.x)/1000;
	this->pMin.y = ((double)pMin.y)/1000;
	this->pMax.x = ((double)pMax.x)/1000;
	this->pMax.y = ((double)pMax.y)/1000;

	// this->proximity_map = new std::map<int, int>();

	this->proximity = 0;
	// this->crashing = 0;

	this->obstaclePoint = Point();
	ros::init(argc, argv, "lidarhandler");
	this->ros_thread = std::thread(&Lidar::runOnThread, this);
}

Lidar::~Lidar() {
	ros::shutdown();
	// delete this->collision_map;
	this->ros_thread.join();
}

char Lidar::checkProximity() {
	std::lock_guard< std::mutex > lock(this->proximity_mutex);
	char p = this->proximity;
	return p;
}

void Lidar::lidarCallback(const sensor_msgs::LaserScan::ConstPtr& scan) {
	double inc = scan->angle_increment;
	double minr = 99999;
	double mina = 99999;
	double minx = 99999;
	double miny = 99999;
	double ang = 0;
	double r = 0;
	double x = 0;
	double y = 0;
	proximity = 0;
	for (int n = 0; n < scan->ranges.size(); n++) {
		r = scan->ranges[n];
		x = r*cos(ang);
		y = r*sin(ang);
		if ( x < this->rMin.x && x > this->pMin.x
			 && y > this->pMin.y && y < this->pMax.y) {
			if (y < this->rMin.y) {
				// Front left
				proximity = proximity | 1;
			}
			else if (y > this->rMax.y) {
				// Front right
				proximity = proximity | 2;
			}
			else {
				// Front
				proximity = proximity | 4;
			}
		}
		else if ( x > this->rMax.x && x < this->pMax.x
			      && y > this->pMin.y && y < this->pMax.y) {
			if (y < this->rMin.y) {
				// Back left
				proximity = proximity | 8;
			}
			else if (y > this->rMax.y) {
				// back right
				proximity = proximity | 16;
			}
			else {
				// back
				proximity = proximity | 32;
			}
		}
		else if ( x > this->rMin.x && x < this->rMax.x
			      && y > this->pMin.y && y < this->pMax.y) {
			if (y < this->rMin.y) {
				// left
				proximity = proximity | 64;
			}
			else if (y > this->rMax.y) {
				// right
				proximity = proximity | 128;
			}
			else {
				// inside robot
			}
		}
		ang += inc;
	}
	std::lock_guard< std::mutex > lock(this->proximity_mutex);
	this->proximity = proximity;
}

static void cb_wrapper(void* objPtr, const sensor_msgs::LaserScan::ConstPtr& scan) {
	Lidar* self = (Lidar*)objPtr;
	self->lidarCallback(scan);
}

void Lidar::runOnThread() {
	ros::NodeHandle handle;
	const boost::function< void(const sensor_msgs::LaserScan::ConstPtr &) > boundCallback = boost::bind(&Lidar::lidarCallback, this, _1);
	ros::Subscriber sub = handle.subscribe("/scan", MSG_QUEUE_SIZE, boundCallback);
	ros::spin();
}
